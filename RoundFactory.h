#ifndef ROUND_FACTORY_H
#define ROUND_FACTORY_H

// Class for creating rounds. Create round every <interval> seconds with radius [minRadius,maxRadius] from GameConfig.

class Round;

class RoundFactory
{
public:
    RoundFactory(int width, int height);

    // Returns new round, or 0 if time < interval.
    Round *create(float dt);

private:
    // Returns new round.
    Round *createRound();

private:
    int m_width;
    int m_height;
};

#endif // ROUND_FACTORY_H
