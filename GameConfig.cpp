#include "GameConfig.h"

const float GameConfig::interval = 1.0f;
const float GameConfig::minRadius = 10.0f;
const float GameConfig::maxRadius = 50.0f;

float GameConfig::calculateSpeed(float radius)
{
    return (3*maxRadius - radius);
}

float GameConfig::calculatePoints(float radius)
{
    int points = (int(maxRadius - radius) / 5) * 5;
    return points ? points : 1;
}
