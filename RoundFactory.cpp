#include "RoundFactory.h"

#include <cstdlib>

#include "Round.h"
#include "GameConfig.h"

RoundFactory::RoundFactory(int width, int height)
: m_width(width)
, m_height(height)
{
}

// Returns new round object or 0.
Round *RoundFactory::create(float dt)
{
    static float time = 0;

    time += dt;

    // Create round only if time more than standard game time interval.
    if (time > GameConfig::interval)
    {
        time = 0;
        return createRound();
    }

    return 0;
}

// Returns new round object.
Round *RoundFactory::createRound()
{
    // Determine radius for new round.
    float radius = GameConfig::minRadius + (GameConfig::maxRadius - GameConfig::minRadius) * rand() / RAND_MAX;

    // Determine x position in interval [radius,m_wirth-radius].
    float x = rand() % int(m_width - 2*int(radius+1)) + radius;

    // Randomize color.
    float r = (rand() % 256) / 255.0f;
    float g = (rand() % 256) / 255.0f;
    float b = (rand() % 256) / 255.0f;

    Round *round = new Round(radius, x, m_height + radius, r, g, b);

    return round;
}
