#ifndef GAME_H
#define GAME_H

#include <list>

class Round;
class RoundFactory;

typedef std::list<Round*> RoundsContainer;
typedef std::list<Round*>::iterator RoundsContainerIterator;

/* Main class for game representation.
 * Call update with time interval, touch with touch's coordinate and get rounds for drawing.
 */

class Game
{
public:
    // Create game with size of game field.
    Game(int width, int height);
    ~Game();

    // Update game world with time interval dt.
    void update(float dt);
    // Touch point (x,y).
    void touch(int x, int y);

    // Returns points.
    int points() const;

    // Get all visible rounds for drawing.
    RoundsContainer &rounds();

private:
    int m_width;
    int m_height;
    int m_points;
    RoundsContainer m_rounds;
    RoundFactory *m_factory;
};

#endif // GAME_H
