#include "Round.h"

Round::Round(float radius, float x, float y, float r, float g, float b)
: m_radius(radius)
, m_x(x)
, m_y(y)
, m_r(r)
, m_g(g)
, m_b(b)
{
}

float Round::x() const
{
    return m_x;
}

float Round::y() const
{
    return m_y;
}

float Round::r() const
{
    return m_r;
}

float Round::g() const
{
    return m_g;
}

float Round::b() const
{
    return m_b;
}

void Round::setX(float x)
{
    m_x = x;
}

void Round::setY(float y)
{
    m_y = y;
}

float Round::radius() const
{
    return m_radius;
}

bool Round::isTouched(int x, int y) const
{
    // Fast check of touch - touching the square.
    if ((x < m_x - m_radius) || (x > m_x + m_radius) || (y < m_y - m_radius) || (y > m_y + m_radius))
    {
        return false;
    }

    // Check touching round.
    if ((m_x-x)*(m_x-x) + (m_y-y)*(m_y-y) > m_radius*m_radius)
    {
        return false;
    }

    return true;
}
