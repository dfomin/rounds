#ifndef ROUND_H
#define ROUND_H

// Class Round: radius, position, color. Can check touches.

class Round
{
public:
    Round(float radius, float x, float y, float r, float g, float b);

    // Position
    float x() const;
    float y() const;

    // Color
    float r() const;
    float g() const;
    float b() const;

    float radius() const;

    // Check touch on round or not.
    bool isTouched(int x, int y) const;

    void setX(float x);
    void setY(float y);

private:
    float m_radius;
    float m_x;
    float m_y;
    float m_r;
    float m_g;
    float m_b;
};

#endif // ROUND_H
