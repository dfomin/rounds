#ifndef GAME_CONFIG_H
#define GAME_CONFIG_H

// Class for representing configuration: dependency of sizes, speed, points, update time etc.

struct GameConfig
{
    static float calculateSpeed(float radius);
    static float calculatePoints(float radius);

    static const float interval;
    static const float minRadius;
    static const float maxRadius;
};

#endif // GAME_CONFIG_H
