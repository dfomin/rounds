#include "Game.h"

#include "Round.h"
#include "RoundFactory.h"
#include "GameConfig.h"

Game::Game(int width, int height)
: m_width(width)
, m_height(height)
, m_points(0)
{
    m_factory = new RoundFactory(m_width, m_height);
}

Game::~Game()
{
    delete m_factory;
}

void Game::update(float dt)
{
    Round *newRound = m_factory->create(dt);

    // If new round was created, add it to container.
    if (newRound)
    {
        m_rounds.push_back(newRound);
    }

    RoundsContainerIterator begin = m_rounds.begin();
    RoundsContainerIterator end = m_rounds.end();

    while (begin != end)
    {
        Round *round = *begin;

        // Move round.
        float y = round->y();
        y -= GameConfig::calculateSpeed(round->radius()) * dt;

        if (y < -round->radius())
        {
            // Delete round, which out of screen.
            delete *begin;
            begin = m_rounds.erase(begin);
        }
        else
        {
            round->setY(y);
            ++begin;
        }
    }
}

void Game::touch(int x, int y)
{
    RoundsContainerIterator begin = m_rounds.begin();
    RoundsContainerIterator end = m_rounds.end();

    while (begin != end)
    {
        Round *round = *begin;

        if (round->isTouched(x, y))
        {
            // Add points if round was touched and remove it.
            m_points += GameConfig::calculatePoints(round->radius());

            delete *begin;
            begin = m_rounds.erase(begin);
        }
        else
        {
            ++begin;
        }
    }
}

int Game::points() const
{
    return m_points;
}

RoundsContainer &Game::rounds()
{
    return m_rounds;
}
